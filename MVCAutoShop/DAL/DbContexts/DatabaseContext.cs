﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DbContexts
{
    public class DatabaseContext : IdentityDbContext<User, Role, int>
    {
        private String connectionString = @"Data Source=DESKTOP-STN0GIA\SQLEXPRESS;Initial Catalog=MVCAuto;Trusted_Connection=Yes;";

        public DatabaseContext()
        {

        }

        public DatabaseContext(DbContextOptions<IdentityDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Appointment>()
                .HasIndex(a => a.DateOfAppointment)
                .IsUnique();
            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<Appointment> Appointments { get; set; }
    }
}
