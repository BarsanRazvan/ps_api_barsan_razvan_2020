﻿using BAL.Interfaces;
using BAL.Models;
using BAL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AutoShopAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/appointments")]
    public class AppointmentsController : ControllerBase
    {
        private readonly IAppointmentService _appointmentService;

        public AppointmentsController()
        {
            _appointmentService = new AppointmentService();
        }

        [HttpGet()]
        public ActionResult<IEnumerable<AppointmentModel>> GetAppointments()
        {
            var appointments = _appointmentService.GetAppointments();
            return Ok(new { totalCount = appointments.Count(), appointments });
        }

        [HttpGet("{authorId:int}")]
        public ActionResult<AppointmentModel> GetAppointment(int authorId)
        {

            var appointment = _appointmentService.GetAppointment(authorId);

            if (appointment == null)
            {
                return NotFound();
            }

            return Ok(appointment);
        }
    }
}
