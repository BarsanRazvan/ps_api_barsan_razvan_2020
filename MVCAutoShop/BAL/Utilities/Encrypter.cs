﻿using BAL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BAL.Utilities
{
    class Encrypter
    {
        private Encrypter()
        {

        }

        public static string SHA512(string input)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(input);
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                var hashedInputStringBuilder = new System.Text.StringBuilder(128);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                return hashedInputStringBuilder.ToString();
            }
        }

        public static bool VerifyHashed(string hashedPassword, string password)
        {
            PasswordHasher<UserModel> passwordHasher = new PasswordHasher<UserModel>();
            return passwordHasher.VerifyHashedPassword(new UserModel(), hashedPassword, password) != PasswordVerificationResult.Failed;
        }
    }
}
