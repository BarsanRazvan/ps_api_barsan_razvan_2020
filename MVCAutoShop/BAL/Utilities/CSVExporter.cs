﻿using BAL.Interfaces;
using BAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Utilities
{
    class CSVExporter : Exporter
    {

        public String ExportFile(IEnumerable<AppointmentModel> list)
        {
            StringBuilder sList = new StringBuilder();

            Type type = typeof(AppointmentModel);
            var props = type.GetProperties();
            sList.Append(string.Join(",", props.Select(p => p.Name)));
            sList.Append(Environment.NewLine);

            foreach (var element in list)
            {
                sList.Append(string.Join(",", props.Select(p => p.GetValue(element, null))));
                sList.Append(Environment.NewLine);
            }

            return sList.ToString();
        }
    }
}
