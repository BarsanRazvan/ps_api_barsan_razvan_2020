﻿using BAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace BAL.Utilities
{
    public class ExporterFactory
    {
        private static Dictionary<String, Type> m_RegisteredExporter = new Dictionary<String, Type>();

        static ExporterFactory()
        {
            Type[] typeInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();
            foreach (Type type in typeInThisAssembly)
            {
                if (type.GetInterface(typeof(Exporter).ToString()) != null)
                {
                    m_RegisteredExporter.Add(type.Name, type);
                }
            }
        }

        public Exporter CreateExporter(String exporterID)
        {
            Exporter exporter;
            try
            {
                exporter = (Exporter)Activator.CreateInstance(m_RegisteredExporter[exporterID]);
                return exporter;
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }
    }
}
