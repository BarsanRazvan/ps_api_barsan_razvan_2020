﻿using BAL.Interfaces;
using BAL.Models;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BAL.Utilities
{
    public class JSonExporter : Exporter
    {
        public String ExportFile(IEnumerable<AppointmentModel> list)
        {
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            String result = JsonSerializer.Serialize(list, options);
            return result;
        }
    }
}
