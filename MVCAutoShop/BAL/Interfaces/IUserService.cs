﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface IUserService
    {
        UserModel CheckCredentials(String username, String password);
        UserModel GetUser(int id);
    }
}
