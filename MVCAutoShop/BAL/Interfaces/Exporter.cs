﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BAL.Interfaces
{
    public interface Exporter
    {
        String ExportFile(IEnumerable<AppointmentModel> list);
    }
}
