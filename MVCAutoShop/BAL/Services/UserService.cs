﻿using BAL.Interfaces;
using BAL.Models;
using BAL.Utilities;
using DAL.Entities;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork unitOfWork;

        public UserService()
        {
            unitOfWork = new UnitOfWork();
        }

        public UserModel CheckCredentials(String username, String password)
        {
            User user = unitOfWork.Users.Get(u => u.UserName == username).FirstOrDefault();
            if(Encrypter.VerifyHashed(user.PasswordHash, password))
            {
                return ModelMapper.MapValue<User, UserModel>(user);
            }

            return null;
        }

        public UserModel GetUser(int id)
        {
            User user = unitOfWork.Users.GetByID(id);
            if (user != null)
            {
                return ModelMapper.MapValue<User, UserModel>(user);
            }

            return null;
        }
    }
}
